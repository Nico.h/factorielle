package fr.ajc.formation.java;

public class CalculateurFactorielle {

	public static long factorielle(long entier) {
		if (entier == 0 || entier == 1) {
			return 1;
		} else if (entier > 1) {
			long resultat = 1;
			for (long i = 2; i <= entier; i++)
			{
				resultat *= i;
			}
			return resultat;
		} else {
			return 0;
		}
	}
	
	public static void main(String[] args) {
		System.out.println(CalculateurFactorielle.factorielle(0));
		System.out.println(CalculateurFactorielle.factorielle(3));
		System.out.println(CalculateurFactorielle.factorielle(6));
	}
}
