package fr.ajc.formation.java;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculateurFactorielleTest {

	@Test
	void testSiValeurEntreeNegatif() {
		assertEquals(0, CalculateurFactorielle.factorielle(-2));
	}
	
	@Test
	void testSiValeurEntreeNulle() {
		assertEquals(1, CalculateurFactorielle.factorielle(0));
	}
	
	@Test
	void testSiValeurEntreeEgaleUn() {
		assertEquals(1, CalculateurFactorielle.factorielle(1));
	}
	
	@Test
	void testSiValeurEntreeVautDix() {
		assertEquals(3628800, CalculateurFactorielle.factorielle(10));
	}
	
	@Test
	void test_factorielle_3_egal_6() {
		assertEquals(6, CalculateurFactorielle.factorielle(3));
	}
	
	@Test
	void test_factorielle_6_egal_720() {
		assertEquals(720, CalculateurFactorielle.factorielle(6));
	}

}
